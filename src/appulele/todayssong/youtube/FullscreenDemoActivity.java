/*
 * Copyright 2012 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package appulele.todayssong.youtube;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import appulele.todayssong.R;
import appulele.todayssong.json.GetYoutubeIDTask;
import appulele.todayssong.json.JSONTask;
import appulele.todayssong.json.LyricsTask; 
import appulele.todayssong.json.URLs;
import appulele.todayssong.json.webviewtohtml.MyJavaScriptInterface;

import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

/**
 * Sample activity showing how to properly enable custom fullscreen behavior.
 * <p>
 * This is the preferred way of handling fullscreen because the default
 * fullscreen implementation will cause re-buffering of the video.
 */
@SuppressLint("JavascriptInterface")
public class FullscreenDemoActivity extends YouTubeFailureRecoveryActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, YouTubePlayer.OnFullscreenListener {

	private static final String TAG ="FullDemoAct";
	private static final int PORTRAIT_ORIENTATION = Build.VERSION.SDK_INT < 9 ? ActivityInfo.SCREEN_ORIENTATION_PORTRAIT : ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT;
	private LinearLayout baseLayout;
	private YouTubePlayerView playerView;
	private YouTubePlayer player;
	private Button fullscreenButton;
	private CompoundButton checkbox;
	private View otherViews;
	// default MOVIE 아이디
	private String MOVIEID = "kWjRB9SmMss";
	private boolean fullscreen;
	TextView tvMovieName;
	TextView tvLyrics;
	String findText;
	String songId;
	String songName;
	String singerName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.fullscreen_demo);
		Intent intent = getIntent();
		findText = intent.getStringExtra("findText");
		songId = intent.getStringExtra("songId"); 
		songName = intent.getStringExtra("songName");
		singerName = intent.getStringExtra("singerName");

		// JSON 이용해서 유투브검색 , 연결하기

		tvLyrics = (TextView) findViewById(R.id.tvLycs);
		tvMovieName = (TextView) findViewById(R.id.tvYoutubeMovieName);
		tvMovieName.setText(singerName+" - "+ songName);
	
		//youtube api로 유툽 불러오기
		JSONTask jsonTask = new JSONTask(this, findText); 
		jsonTask.execute();
		 
		
		//가사
		LyricsTask lyricsTask = new LyricsTask(this, songId);
		lyricsTask.execute();
//		final WebView wv = new WebView(this);
//		// ////.... webview통해 html 불러오기 .../////////////
//		MyJavaScriptInterface jsInterface = new MyJavaScriptInterface(this);
//		wv.addJavascriptInterface(jsInterface, "HTMLOUT");
//		Log.d(TAG, "웹뷰 start;" );
//		/* WebViewClent must be set BEFORE calling loadUrl! */
//
//		wv.setWebViewClient(new WebViewClient() {
//			@Override
//			public void onPageFinished(WebView view, String url) {
//				/*
//				 * This call inject JavaScript into the page which just finished
//				 * loading.
//				 */
//				Log.d(TAG, "웹뷰 페이지 종료시" );
//				wv.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
//			}
//		}); 
//		wv.getSettings().setJavaScriptEnabled(true);
//		/* load a web page */
//		String queryURL = 	  URLs.youtubeWebFindAQuery + findText;
//		wv.loadUrl(queryURL); 
//		Log.d(TAG, "웹뷰 불러오기 ;" );
		// /////////////
 
	}

	@Override
	public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
		this.player = player;
		setControlsEnabled();
		// Specify that we want to handle fullscreen behavior ourselves.
		player.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
		player.setOnFullscreenListener(this);
		if (!wasRestored) {

			player.cueVideo(MOVIEID);
		}
	}

	@Override
	protected YouTubePlayer.Provider getYouTubePlayerProvider() {
		return playerView;
	}

	@Override
	public void onClick(View v) {
		player.setFullscreen(!fullscreen);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		int controlFlags = player.getFullscreenControlFlags();
		if (isChecked) {
			// If you use the FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE,
			// your activity's normal UI
			// should never be laid out in landscape mode (since the video will
			// be fullscreen whenever the
			// activity is in landscape orientation). Therefore you should set
			// the activity's requested
			// orientation to portrait. Typically you would do this in your
			// AndroidManifest.xml, we do it
			// programmatically here since this activity demos fullscreen
			// behavior both with and without
			// this flag).
			setRequestedOrientation(PORTRAIT_ORIENTATION);
			controlFlags |= YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE;
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
			controlFlags &= ~YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE;
		}
		player.setFullscreenControlFlags(controlFlags);
	}

	private void doLayout() {
		LinearLayout.LayoutParams playerParams = (LinearLayout.LayoutParams) playerView.getLayoutParams();
		if (fullscreen) {
			// When in fullscreen, the visibility of all other views than the
			// player should be set to
			// GONE and the player should be laid out across the whole screen.
			playerParams.width = LayoutParams.MATCH_PARENT;
			playerParams.height = LayoutParams.MATCH_PARENT;

			otherViews.setVisibility(View.GONE);
		} else {
			// This layout is up to you - this is just a simple example
			// (vertically stacked boxes in
			// portrait, horizontally stacked in landscape).
			otherViews.setVisibility(View.VISIBLE);
			ViewGroup.LayoutParams otherViewsParams = otherViews.getLayoutParams();
			if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
				playerParams.width = otherViewsParams.width = 0;
				playerParams.height = WRAP_CONTENT;
				otherViewsParams.height = MATCH_PARENT;
				playerParams.weight = 1;
				baseLayout.setOrientation(LinearLayout.HORIZONTAL);
			} else {
				playerParams.width = otherViewsParams.width = MATCH_PARENT;
				playerParams.height = WRAP_CONTENT;
				playerParams.weight = 0;
				otherViewsParams.height = 0;
				baseLayout.setOrientation(LinearLayout.VERTICAL);
			}
			setControlsEnabled();
		}
	}

	private void setControlsEnabled() {
		checkbox.setEnabled(player != null && getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT);
		fullscreenButton.setEnabled(player != null);
	}

	@Override
	public void onFullscreen(boolean isFullscreen) {
		fullscreen = isFullscreen;
		doLayout();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		doLayout();
	}

	/**
	 * 가사를 뿌려준다.
	 */
	public void spreadText(String lyrics) {
		tvLyrics.setText(lyrics);

	}

	/**
	 * 유튜브 플레이어를 원하는 ID를 넣어서 만든다.
	 * 
	 * @param curID
	 */
	public void sendYouyubeURL(String curID) {
		MOVIEID = curID;
		baseLayout = (LinearLayout) findViewById(R.id.layout);
		playerView = (YouTubePlayerView) findViewById(R.id.player);
		fullscreenButton = (Button) findViewById(R.id.fullscreen_button);
		checkbox = (CompoundButton) findViewById(R.id.landscape_fullscreen_checkbox);
		otherViews = findViewById(R.id.other_views);
		checkbox.setOnCheckedChangeListener(this);
		// You can use your own button to switch to fullscreen too
		fullscreenButton.setOnClickListener(this);

		playerView.initialize(DeveloperKey.DEVELOPER_KEY, this);
		doLayout();
	}
}
