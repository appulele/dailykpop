package appulele.todayssong;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import appulele.todayssong.youtube.FullscreenDemoActivity;

/**
 * AsynkTask 로 만들어진 신곡 DATA LIST를 이용하여, 페이지를 만든다.
 */
public class AddViewPage implements OnClickListener {
	public static final String ARG_SECTION_NUMBER = "section_number";
	private int pageindex;
	Context mContext;
	private SongData songData;
	MainActivity mainActivity;
	private Button btnYoutubePage;
	LayoutInflater inflater;
	private LinearLayout linearPage;
	TextView tvSingerName;
	TextView tvNextSongName;
	TextView tvBeforeSongName;
	// TextView tvAlbumName;
	TextView tvSongName;
	TextView tvDate;
	ImageView ivSong;
	ImageView ivNextSinger;
	ImageView ivBeforeSinger;
	String[] artistId;
	String songId;
	
	// LinearLayout llMid;
	// WebView wvSingerImgFind;

	public AddViewPage(final Context mContext, SongData data,int pageIndex) {
		this.pageindex = pageIndex;
		this.mContext = mContext;
		this.setSongData(data);
		Log.d("tag", "songData:" + getSongData().getSingerName());
		// llMid = (LinearLayout) linearPage.findViewById(R.id.llMid);
		// tvAlbumName = (TextView) linearPage.findViewById(R.id.tvAlbumName);
		// tvAlbumName.setText(songData.getAlbumName());
		mainActivity = (MainActivity) mContext;
		inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		setLinearPage((LinearLayout) inflater.inflate(R.layout.view_main, null));
		linearPage.setTag(pageIndex);
		tvSingerName = (TextView) linearPage.findViewById(R.id.tvSingerName);
		tvDate = (TextView) linearPage.findViewById(R.id.tvDate);
		tvSongName = (TextView) linearPage.findViewById(R.id.tvSongName);
		ivSong = (ImageView) linearPage.findViewById(R.id.imageViewAlbumImage);
		tvDate.setText(getSongData().getAlbumDate());
		tvSingerName.setText(getSongData().getSingerName());
		tvSongName.setText(getSongData().getSongName());
		tvNextSongName = (TextView) linearPage.findViewById(R.id.tvNextSongName);
		tvBeforeSongName = (TextView) linearPage.findViewById(R.id.tvBeforeSongName);
		ivBeforeSinger = (ImageView) linearPage.findViewById(R.id.imageViewBeforeSingerImage);
		ivNextSinger = (ImageView) linearPage.findViewById(R.id.imageViewNextSingerImage);
		ivBeforeSinger.setOnClickListener(this);
		ivNextSinger.setOnClickListener(this);
		ivSong.setOnClickListener(this);

		//
		songId = getSongData().getSongURL();
		artistId = getSongData().getSingerURLs();
		Log.d("", "추적1 artistID[0]:" + artistId[0]);
		
		ivSong.setImageDrawable(getSongData().getSingerImg());
		ivBeforeSinger.setImageDrawable(getSongData().getBeforeSingerImg());
		ivNextSinger.setImageDrawable(getSongData().getNextSingerImg());

		setBtnYoutube((Button) getLinearPage().findViewById(R.id.buttonYoutube));
		getBtnYoutube().setOnClickListener(this);

		mainActivity.getViewFlipper().setAnimationCacheEnabled(false);
		mainActivity.getViewFlipper().addView(linearPage);
		mainActivity.getViewFlipper().setAnimationCacheEnabled(true);
	}

	@Override
	public void onClick(View view) {
		Intent intent;
		switch (view.getId()) {
		// 가수 얼굴 클릭시 이미지 앨범 액티비티
		case R.id.imageViewAlbumImage:
			intent = new Intent(mContext, ArtistImageViewActivity.class);
			intent.putExtra("artistIds", artistId);

			((MainActivity) mContext).startActivityForResult(intent, 0);
			break;

		case R.id.imageViewBeforeSingerImage:
			mainActivity.MovePreviousView();
			break;

		case R.id.imageViewNextSingerImage:
			mainActivity.MoveNextView();
			break;

		case R.id.buttonYoutube:
			intent = new Intent(mContext, FullscreenDemoActivity.class);
			intent.putExtra("songId", songId);
			intent.putExtra("singerName", getSongData().getSingerName());
			intent.putExtra("songName", getSongData().getSongName());
			intent.putExtra("findText", getSongData().getSingerName() + getSongData().getSongName());
			
			mContext.startActivity(intent);
			break;

		default:
			break;
		}
	}

	public View getView() {
		return getLinearPage();
	}

	public Button getBtnYoutube() {
		return btnYoutubePage;
	}

	public void setBtnYoutube(Button btnYoutube) {
		this.btnYoutubePage = btnYoutube;
	}

	public LinearLayout getLinearPage() {
		return linearPage;
	}

	public void setLinearPage(LinearLayout linearPage) {
		this.linearPage = linearPage;
	}

	public int getPageindex() {
		return pageindex;
	}

	public void setPageindex(int pageindex) {
		this.pageindex = pageindex;
	}

	public SongData getSongData() {
		return songData;
	}

	public void setSongData(SongData songData) {
		this.songData = songData;
	}

}
