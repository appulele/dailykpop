package appulele.todayssong;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

class GridAdapter extends BaseAdapter implements OnClickListener {
	LayoutInflater inflater;
	Context mContext;
	private ArrayList<DrawableData> list;

	public GridAdapter(Context mContext, ArrayList<DrawableData> list) {
		this.mContext = mContext;

		inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.setList(list);
	}

	@Override
	public int getCount() {

		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.artist_img_item, parent, false);
		}
		ImageView ivItem = (ImageView) convertView.findViewById(R.id.ivImgItem);
		ivItem.setImageDrawable(list.get(position).getImg());
		ivItem.setOnClickListener(this);
		return convertView;
	}

	@Override
	public void onClick(View view) { 
		ImageView ivClicked = (ImageView) view; 
		Intent intent = new Intent(mContext, ArtistImageFullViewActivity.class); 
		int index = getDrawableImdex(true,ivClicked.getDrawable());
		intent.putExtra("index",index );
		index = getDrawableImdex(false,ivClicked.getDrawable());
//		Log.d("","누른 이미지의 id는 = "+list.get(index).getSingerIds()[index]+"   , 누른 이미지의 이름은 "+list.get(index).getSingerNames()  );
		
	  intent.putExtra("singerIndex",list.get(index).getSingerIndex() );
		ArrayList<String> singerNames = list.get(index).getSingerNames(); 
		 
		intent.putExtra("artistIDs", list.get(index).getSingerIds()); 
		String[] array = singerNames.toArray(new String[list.size()]);
		intent.putExtra("artistNames", array);
 
		
		mContext.startActivity(intent);
	}

	 /**
	  * check이 true일 경우 곡의 index를, false일 경우에는 전체에서의 index를 제공 
	  * @param check
	  * @param img
	  * @return
	  */
	public int getDrawableImdex(boolean check, Drawable img) {

		int index=0;
		for (int i = 0; i < list.size(); i++) {
			if(list.get(i).getImg().equals(img)){
				return index;
			}else{
				index++;
			}
	
			if(check&&i<list.size()&&list.get(i).getSingerIndex()!=list.get(i+1).getSingerIndex()){
				index=0;
			}
		}

		return index;
	}
	
 

	public ArrayList<DrawableData> getList() {
		return list;
	}

	public void setList(ArrayList<DrawableData> list) {
		this.list = list;
	}

}