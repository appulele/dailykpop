package appulele.todayssong.json;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.R.drawable;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import appulele.todayssong.ArtistImageViewActivity;
import appulele.todayssong.DrawableData;
import appulele.todayssong.MainActivity;
import appulele.todayssong.R;
import appulele.todayssong.SongData;
import appulele.todayssong.extermalImg.externalDrawable;

/**
 * 아티스트 숫자만큼 (2명이라면) artistID를 가지고 그 아티스트 각각의 Drawable ArrayList로 이미지를 가진다. 그리고
 * 그 이미지를 addItem해준다.
 * 
 * @author sanghyeon
 * 
 */
@SuppressLint("SetJavaScriptEnabled")
public class OuterArtistImageUrlTask extends AsyncTask<Void, Void, Void> {
	/**
	 * 현재 곡의 아티스트 이미지를 불러온다.
	 * */
	private final String TAG = this.getClass().getSimpleName();
	private final boolean D = false;
	private Context mContext;
	ArtistImageViewActivity activity;
	// 가수 정보를 불러온다
	String[] artistIDs;
	public static ArrayList<DrawableData> drawableArray;
	ArrayList<Document> docList;
	static ArrayList<String> singerNames;

	public OuterArtistImageUrlTask(Context mContext, String[] artistIDs) {
		this.mContext = mContext;
		activity = (ArtistImageViewActivity) mContext;
		this.artistIDs = artistIDs;
		docList = new ArrayList<>();
		singerNames = new ArrayList<>();
		drawableArray = new ArrayList<>();
	}

	/**
	 * 가수(들)의 이미지를 불러와서 item으로 어댑터에 추가.
	 */
	@Override
	protected Void doInBackground(Void... params) {

		for (int i = 0; i < artistIDs.length; i++) {
			getSingerImgs(artistIDs);
		}
		return null;
	}

	// Post Execute
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		Log.d("t","addImg 전신 첫번째 이름은: "+drawableArray.get(0).getSingerNames());
		
		activity.addImg(drawableArray);

	}

	private ArrayList<Drawable> getSingerImgs(String[] ids) {

		for (int j = 0; j < ids.length; j++) {
			String url = URLs.artistURL + ids[j];
			Document doc = null;
			// for (int i = 0; i < artistIDs.length; i++) {
			try {
				doc = Jsoup.connect(url).get();
				docList.add(doc);
				Log.d(TAG, "artistphoto url is " + url);
				Log.d(TAG, "artistphoto doc:" + doc.text());

			} catch (IOException e) {
				e.printStackTrace();
			} 
			// }

			Elements ImgURLsData = doc.getElementsByClass("photo-list");
			String singerName = doc.getElementsByClass("info").select("dt").text();
			if( singerNames.contains(singerName)){
				 Log.d(TAG,"존재하는 가수이름"+singerNames.size()+singerName);
			 }else{
				 singerNames.add(singerName);
				 Log.d(TAG,"존재하지 않는 가수이름"+singerNames.size()+singerName);
				 
					int size = ImgURLsData.select("li").size();
					for (int i = 0; i < size; i++) {
						String ImgURL = ImgURLsData.select("li").get(i).select("img").attr("src");
						Log.d(TAG, "singerName:" + singerName);
						Drawable drawable = externalDrawable.LoadImageFromWebOperations(ImgURL);
						DrawableData drawableData = new DrawableData(drawable, i, ids,j);
						drawableData.addSingerNames(singerNames);
						drawableArray.add(drawableData);
						Log.d("t","addImg 전신 첫번째 이름은: "+drawableArray.get(0).getSingerNames().get(0));
					}
			 }
		
		}

		return null;
	}
}
