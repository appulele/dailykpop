package appulele.todayssong.json;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import appulele.todayssong.youtube.FullscreenDemoActivity;

public class JSONTask extends AsyncTask<String, String, String> {
	protected void onPreExecute() {
	}

	String TAG = getClass().getSimpleName();
	FullscreenDemoActivity activity;
	String needs;
	String convertHTML;
	String queryURL;

	public JSONTask(Context mContext, String needs) {
		activity = (FullscreenDemoActivity) mContext;
		this.needs = needs;
		Log.d(TAG,"needs 생성자에서는:"+needs);
	}

	@Override
	protected String doInBackground(String... urls) {
		// queryURL = URLS.YOUTUBEFINDAQUERY + NEEDS + URLS.YOUTUBEFINDBMAX + 3
		// + URLS.YOUTUBEFINDC;
		try {
			needs = URLEncoder.encode(needs, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		queryURL = URLs.youtubeAPIFindAQuery + needs + URLs.youtubeAPIFindBMax + 1 + URLs.youtubeAPIFindC;
		// queryURL = queryURL.replaceAll(" ", "%20");
		convertHTML = convertHTML(queryURL);
		Log.d(TAG,"needs 이용한 쿼리 URL은 "+queryURL);
		return convertHTML;
	}

	protected void onPostExecute(String convertHTML) {
		// json['data']['items'][0]['id']
		String name = null;
		String age = null;
String curID ="kWjRB9SmMss";
		Log.d(TAG, "ConvertHTMLS:" + convertHTML);
		JSONObject jsonObj;
		try {
			JSONObject jo = new JSONObject(convertHTML);
			JSONObject jsonObjectData = jo.getJSONObject("data");
			JSONArray jsonArrayItems = jsonObjectData.getJSONArray("items");
			for(int i=0; i<jsonArrayItems.length(); i++){
				JSONObject curJsonObjectItem = jsonArrayItems.getJSONObject(i);
				String curItemId = curJsonObjectItem.getString("id");
				Log.d(TAG, i + "번째 Item ID : " + curItemId);
				curID = curItemId;
			}
			
		} catch (JSONException e) {
			 
		 //에러시에 일단 보여주는 영상
			activity.sendYouyubeURL("kWjRB9SmMss");
			e.printStackTrace();
		}

	 

		activity.sendYouyubeURL(curID);

	}

	private String convertHTML(String queryURL) {
		String originalHTML = null;
		Document doc = null;

		// 해당 url의 HTML 받아옴

		try {
			originalHTML = Jsoup.connect(queryURL).ignoreContentType(true).execute().body();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// try {

		// } catch (Exception e) {
		// return "<h1> 응답이 없습니다, 다시 시도 해 주세요. </h1>";
		// }

		return originalHTML;
	}
}
