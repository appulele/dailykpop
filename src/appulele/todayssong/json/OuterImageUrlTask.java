package appulele.todayssong.json;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.R.drawable;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import appulele.todayssong.ArtistImageFullViewActivity;
import appulele.todayssong.MainActivity;
import appulele.todayssong.R;
import appulele.todayssong.SongData;
import appulele.todayssong.extermalImg.externalDrawable;

@SuppressLint("SetJavaScriptEnabled")
public class OuterImageUrlTask extends AsyncTask<Void, Void, Void> {
	/**
	 * 받아온 songDataList의 빈 내용을 채워 넣는다.
	 * */
	private final String TAG = this.getClass().getSimpleName();
	private final boolean D = false;
	private Context mContext;
	ArrayList<SongData> songDataList;

	MainActivity mainActivity;
	ViewFlipper viewFlipper;

	public OuterImageUrlTask(Context mContext, ArrayList<SongData> songDataList) {
		mainActivity = (MainActivity) mContext;
		this.songDataList = songDataList;
		viewFlipper = mainActivity.getViewFlipper();

	}

	/**
	 * songDataList의 리스트의 숫자만큼 각각의 이미지url, 발매일등의 정보를 갱신한다. B N A 세가지
	 * Drawable저장해야되고, 따라서 0 일때 0이 아닐 때, 마지막일 때 0이 아닐 때, 자신의 N B 채우고 이전것의 A 채운다.
	 * 마지막 일 때, 자신의 N B 채우고 0의 N을 A로 채운다. 0의 B를 자신의 N으로 채운다. 0일 때, 자신의 N 채운다.
	 */
	@Override
	protected Void doInBackground(Void... params) {
		for (int i = 0; i < songDataList.size(); i++) {
			SongData data = songDataList.get(i);

			Drawable drawable = externalDrawable.LoadImageFromWebOperations(data.getAlbumURL());
			// 자기 자신 싱어 이미지
			data.setSingerImg(drawable);

			if (i != 0) {
				SongData beforeData = songDataList.get(i - 1);
				data.setBeforeSingerImg(beforeData.getSingerImg());
				beforeData.setNextSingerImg(drawable);
			}
			if (i == songDataList.size() - 1) {
				SongData firstData = songDataList.get(0);
				data.setNextSingerImg(firstData.getSingerImg());
				firstData.setBeforeSingerImg(drawable);
			}
			// ivSinger.setImageDrawable(drawable);
		}
		return null;
	}

	// Post Execute
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);

		mainActivity.addViewFlipperPage(songDataList);
		 
	}
}
