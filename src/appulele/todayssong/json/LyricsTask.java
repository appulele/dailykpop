package appulele.todayssong.json;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Toast;
import appulele.todayssong.LoadingActivity;
import appulele.todayssong.MainActivity;
import appulele.todayssong.SongData;
import appulele.todayssong.youtube.FullscreenDemoActivity;

@SuppressLint("SetJavaScriptEnabled")
public class LyricsTask extends AsyncTask<Void, Void, Void> {

	/**
	 * 노래의 SongID를 받아 가사를 불러온다. 이후 가사를 번역한다.
	 */
	private final String TAG = this.getClass().getSimpleName();
	private final boolean D = false;
	private Context mContext;
	private String lyrics;
	String lyricsURL;
	String translateLyrics;
	FullscreenDemoActivity fullscreenActivity;

	public LyricsTask(Context mContext, String songId) {

		this.mContext = mContext;
		fullscreenActivity = (FullscreenDemoActivity) mContext;

		lyricsURL = URLs.lyricsURL + songId;
	}

	@Override
	protected Void doInBackground(Void... params) {
		lyrics = getLyrics(lyricsURL); 
		Log.d(TAG, lyrics);
		return null;
	}

	// Post Execute
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);

		fullscreenActivity.spreadText(lyrics);

	}

	private String getLyrics(String lyricsURL) {
		String originalHTML;
		Document doc = null;
		// 해당 url의 HTML 받아옴
		try {
			doc = Jsoup.connect(lyricsURL).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			originalHTML = doc.html();

		} catch (Exception e) {
			return "가사를 찾을 수 없습니다.";
		}
		lyrics = doc.getElementsByClass("lyrics").html().replaceAll("<br />", "");
		
//		translateLyrics = getTranslateLyrics(lyrics,URLs.KOStart,URLs.EnEnd);  
//		return translateLyrics;
		return lyrics;
	}
	
	/**  Bing tras 로 변경
	 *  번api를 통해 한글->일본어-> 원하는 언어로 번역( 한국어, 일본어, 영어, 프랑스어, etc동남아어 등등)
	 * @param convertLyrics
	 * @return
	 */
	private String getTranslateLyrics(String convertLyrics, String conturyStartCode, String conturyEndCode) {
		String originalHTML;
		Document doc = null;
		String transLyrics;
		String transURL = URLs.translateURL+conturyStartCode+conturyEndCode+convertLyrics;
		// 해당 url의 HTML 받아옴
		try {
			doc = Jsoup.connect(transURL).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			originalHTML = doc.html();

		} catch (Exception e) {
			return "가사를 찾을 수 없습니다.";
		}
		transLyrics = doc.getElementsByClass("hps").html();
		Log.d(TAG,"trans:"+translateLyrics+",  lyricsURL :"+transURL);
		return transLyrics;
	}

}
