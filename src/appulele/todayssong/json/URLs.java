package appulele.todayssong.json;

public class URLs {

	public static String songListURL = "http://m.melon.com/cds/newmusic/mobile2/newsong_list.htm";
	public static String songIDURL = "http://m.melon.com/cds/song/mobile2/songdetailmain_list.htm?songId=";
	public static String artistURL = "http://m.melon.com/cds/artist/mobile2/artistphoto_list.htm?artistId=";
	public static String fullSizeImgUrlA = "http://m.melon.com/cds/artist/mobile2/artistphoto_view.htm?artistId=";
	public static String fullSizeImgUrlB = "&photoNum=";
	
	//api로 호출시 
	public static String youtubeAPIFindAQuery = "http://gdata.youtube.com/feeds/api/videos?q=";
	public static String youtubeAPIFindBMax = "&max-results=";
	public static String youtubeAPIFindC = "&region=KR&ends-before=2014&alt=jsonc&v=2";
//	public static String youtubeFindC = "&alt=jsonc&v=2&autoplay=1&loop=1";
	
	//web 파싱으로 호출시
	//
	public static String youtubeWebFindAQuery = "http://m.youtube.com/results?search_query=";
	public static String youtubeWebFindBFilter = "&filters=week&lclk=week";
	

		
	public static String lyricsURL = "http://m.melon.com/cds/song/mobile2/songdetaillyric_list.htm?songId=";
	  
	public static String translateURL = "http://translate.google.co.kr";
	public static String KOStart = "/#ko";
	public static String JPEnd = "/ja/";
	public static String EnEnd = "/en/";
	
	 
	
}
