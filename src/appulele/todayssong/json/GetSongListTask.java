package appulele.todayssong.json;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Toast;
import appulele.todayssong.LoadingActivity;
import appulele.todayssong.MainActivity;
import appulele.todayssong.SongData;

@SuppressLint("SetJavaScriptEnabled")
public class GetSongListTask extends AsyncTask<Void, Void, Void> {
 
	/**
	 * 신곡 리스트를 불러와서 앨범ID, 신곡 ID를 저장한다.
	 */
	private final String TAG = this.getClass().getSimpleName();
	private final boolean D = false;
	private Context mContext;
	private String convertHTML;
	ArrayList<SongData> songDataList;
	LoadingActivity loadingActivity ;
	
	public GetSongListTask(Context mContext) {
		this.mContext = mContext;
		loadingActivity = (LoadingActivity)mContext; 
		songDataList = new ArrayList<SongData>();
	}

	@Override
	protected Void doInBackground(Void... params) {
		convertHTML = convertHTML(URLs.songListURL);
		
		Log.d(TAG, convertHTML);
		return null;
	}

	// Post Execute
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		
		loadingActivity.addSongList(songDataList);
		
	
	}

	private String convertHTML(String wikiURL) {
		String originalHTML;
		Document doc = null;

		// 해당 url의 HTML 받아옴
		try {
			doc = Jsoup.connect(wikiURL).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// 연결이 안되는 등 문제가 생기면 알림(타임아웃 인터넷 꺼놓음 등 )

		try {
			originalHTML = doc.html();

		} catch (Exception e) {
			return "<h1> 응답이 없습니다, 다시 시도 해 주세요. </h1>";
		}

		// 전체 리스트 불러오기
		Elements songlist = doc.getElementsByClass("thumb-list");

		// Elements href = doc.getElementsByClass("rdt-thumb");
		// Elements href = doc.getElementsByClass("thumb");

		Elements songName = songlist.select("dt");
		Elements singerName = songlist.select("dd");
		Elements albumURL = doc.getElementsByClass("rdt-thumb");
		Elements songURL = doc.getElementsByClass("rdt-song");
		SongData data = null;
		// 노래, 가수
		for (int i = 0; i < songName.size(); i++) {
			data = new SongData(songName.get(i).html(), singerName.get(i).html());
			songDataList.add(data);
		}
		/*
		 * // 노래 for (Element song : songName) { Log.d(TAG, "hrefSong:" +
		 * song.html() + ",   " + song.ownText() + ",   " + song.text());
		 * 
		 * } 가수 for (int i = 0; i <singerName.size(); i++) { Element song =
		 * singerName.get(i); data = songDataList.get(i);
		 * 
		 * Log.d(TAG, "hrefSinger:" + song.html() + ",   " + song.ownText() +
		 * ",   " + song.text());
		 * 
		 * }
		 */
		
		// 노래주소
		for (int i = 0; i < songURL.size(); i++) {
			// for (Element song : songURL) {
			Element song = songURL.get(i);
			String[] split = song.attr("abs:href").split("songId=");
			String url = split[1];
			data = songDataList.get(i);
			data.setSongURL(url);
			Log.d(TAG, "href songID:" + url);
		}
		// 앨범주소
		// for (Element song : albumURL) {
		for (int i = 0; i < albumURL.size(); i++) {
			Element song = albumURL.get(i);
			String[] split = song.attr("abs:href").split("albumId=");
			String url =split[1];
			data = songDataList.get(i);
			data.setAlbumURL(url);
			Log.d(TAG, "href AlbumID:" + url);
		}

		return originalHTML;

	}

}
