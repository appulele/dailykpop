package appulele.todayssong.json;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.android.youtube.player.internal.d;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Toast;
import appulele.todayssong.LoadingActivity;
import appulele.todayssong.MainActivity;
import appulele.todayssong.SongData;

@SuppressLint("SetJavaScriptEnabled")
public class GetSongDataTask extends AsyncTask<Void, Void, Void> {
	/**
	 * 받아온 songDataList의 빈 내용을 채워 넣는다.
	 * */
	private final String TAG = this.getClass().getSimpleName();
	private final boolean D = false;
	private Context mContext;
	ArrayList<SongData> songDataList;
	LoadingActivity loadingActivity;

	public GetSongDataTask(Context mContext, ArrayList<SongData> songDataList) {
		loadingActivity = (LoadingActivity) mContext;
		this.songDataList = songDataList;
	}

	// songDataList의 리스트의 숫자만큼 각각의 이미지url, 발매일등의 정보를 갱신한다.
	@Override
	protected Void doInBackground(Void... params) {
		SongData data = null;
		for (int i = 0; i < songDataList.size(); i++) {
			data = songDataList.get(i);
			data = getSongData(data);
		}

		return null;
	}

	// Post Execute
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);

		// loadingActivity.addSongList(songDataList);
		loadingActivity.startMain();

	}

	private SongData getSongData(SongData data) {
		SongData mData = data;
		Document doc = null;
		
		String url = URLs.songIDURL + mData.getSongURL();
		Log.d(TAG, "album Name url:" + url);
		try {
			doc = Jsoup.connect(url).get();
			Log.d(TAG, "album Name Doc:" + doc.text());
		} catch (IOException e) {
			e.printStackTrace();
		}

		// /**
		// * 아티스트 아이디 저장
		// */
		// Elements singerIDData = doc.getElementsByClass("info");
		// String singerId =
		// singerIDData.select("dd").get(1).select("a[href]").attr("abs:href");
		// String[] split = singerId.split("artistId=");
		//
		// Log.d(TAG,"split"+ mData.getSongName()+ " = "+split[0]);
		//
		//

		/**
		 * 현재 곡의 앨범 정보 불러오기 앨범 이름, 앨범 출시 날짜,
		 */
		// Elements albumData = doc.getElementsByClass("thumb-list");
		// String albumName = albumData.select("dt").get(1).html();
		// String albumDate = albumData.select("dd").get(1).html();
		// String albumImgSrc = albumData.select("img").attr("src");
		// try {

		Elements albumData = doc.getElementsByClass("list-album");
		Log.d(TAG, "test:" + albumData.text());
		String albumName = albumData.select("dt").get(0).html();
		String albumDate = albumData.select("dd").get(1).html();
		String albumImgSrc = albumData.select("img").attr("src");

		/**
		 * 가수 이미지, 가수 아이디, 아이디 같은 경우는 dt의 숫자가 곡의 아티스트 숫자를 알 수 있게 해준다.
		 */
		Elements singerData = doc.getElementsByClass("list-artist");
		String singerSrc = singerData.select("img").attr("src");

		int artistNum = singerData.select("li").size();
		String[] singerIdArray = new String[artistNum];
		for (int i = 0; i < singerIdArray.length; i++) {
			singerIdArray[i] = singerData.select("li").get(i).select("a[href]").attr("abs:href");
			String[] splite  = singerIdArray[i].split("artistId=");
			singerIdArray[i] = splite[1];
			Log.d("tag,","arrr"+i+"::"+singerIdArray[i]);
		} 
		// Log.d(TAG, "album Name:" + albumData.size() + ", album date" +
		// albumDate + "album name" + albumName + ", " + albumImgSrc +
		// singerSrc);

		// mData.setAlbumName(albumName);
		mData.setAlbumDate(albumDate);
		mData.setAlbumURL(albumImgSrc);
		mData.setSingerImgURL(singerSrc);
		 mData.setSingerURLs(singerIdArray);

		// } catch (Exception e) {
		// TODO: handle exception
		// }

		return mData;

	}
}
