package appulele.todayssong;

import java.util.ArrayList;
import java.util.zip.Inflater;

import com.mocoplex.adlib.AdlibConfig;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import appulele.todayssong.json.OuterArtistFullSizeImageUrlTask;
import appulele.todayssong.json.OuterArtistImageUrlTask;
import appulele.todayssong.json.URLs;

public class ArtistImageFullViewActivity extends BaseActivity implements OnClickListener {
	String packageName;
	String idAdlibr;
	Context mContext;
	GridView gvGallery;
	GridAdapter adapter;
	LinearLayout llSelectArtist;
	// ImageView ivFull;
	WebView wv;
	String[] artistIDs;
	String[] artistName;
	int singerIndex;
	int position;

	ArrayList<DrawableData> drawableList;
	// 음... 처리 어케하는게 좋을 지 고민된다. 걍 일단 static 으로 처리
	public static ArrayList<SongData> songDataList;
	String TAG = "fullView";

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intent = getIntent();
		artistName = intent.getStringArrayExtra("artistNames");
		artistIDs = intent.getStringArrayExtra("artistIDs");

		singerIndex = intent.getIntExtra("singerIndex", 0);
		position = intent.getIntExtra("index", 1) + 1;

		// Log.d(TAG,"artistName count: "+artistName.length+" , artistID:"+artistID.length+" .  position:"
		// +position );
		ActionBar actionBar = getActionBar();
		actionBar.hide();
		setContentView(R.layout.artist_img_fullview);
		packageName = getPackageName();
		idAdlibr = getResources().getString(R.string.ad_adlibr);
		mContext = this;
		// ivFull = (ImageView) findViewById(R.id.ivFullImg);
		// drawableList = OuterArtistImageUrlTask.drawableArray;
		wv = (WebView) findViewById(R.id.wvFullImg);

		llSelectArtist = (LinearLayout) findViewById(R.id.llFullImgSelectSinger);
		// artistID만큼 버튼을 만들고 default view로 0아티스트, 버튼을 누르면 버튼의 artist 이미지를 본다.

		// 버튼 추가해서 가수들 서로 바꿔 슬라이드
		// for (int i = 0; i < artistIDs.length; i++) {
		//
		// LayoutInflater inflater = (LayoutInflater)
		// mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		// Button btn = (Button) inflater.inflate(R.layout.btn_select_artist,
		// null);
		// btn.setText("iD:" + artistIDs + "name:" + artistName);
		// btn.setOnClickListener(this);
		// llSelectArtist.addView(btn);
		//
		// }
		Log.d("artist", "전체화면URL은 :+ " + artistIDs[singerIndex] + "position: " + (position));
		String fullURL = URLs.fullSizeImgUrlA + artistIDs[singerIndex] + URLs.fullSizeImgUrlB + position;
		Log.d("artist", "artistid:+ fullurl/:" + fullURL);
		WebSettings wvSettings = wv.getSettings();
		wvSettings.setLoadWithOverviewMode(true);
		wvSettings.setUseWideViewPort(true);
		wv.getSettings().setJavaScriptEnabled(true);
		Log.d("artist", "전체화면URL은 :+ " + fullURL);
		wv.loadUrl(fullURL);

		// 로딩 전에 작은 이미지를 먼저 불러서 보여주기
		// Drawable img = drawableList.get(position).getImg();
		// ivFull.setImageDrawable(img);
		// OuterArtistFullSizeImageUrlTask task = new
		// OuterArtistFullSizeImageUrlTask(mContext,drawableList,position);
		initAds();

	}

	// 광고 초기화 함수
	protected void initAds() {
		// AdlibConfig.getInstance().bindPlatform("INMOBI", packageName +
		// ".ads.SubAdlibAdViewInmobi");
		// 쓰지 않을 광고플랫폼은 삭제해주세요.
		// AdlibConfig.getInstance().bindPlatform("ADMOB","lhy.undernation.ads.SubAdlibAdViewAdmob");
		// AdlibConfig.getInstance().bindPlatform("TAD","lhy.undernation.ads.SubAdlibAdViewTAD");
		// AdlibConfig.getInstance().bindPlatform("NAVER","lhy.undernation.ads.SubAdlibAdViewNaverAdPost");
		// AdlibConfig.getInstance().bindPlatform("SHALLWEAD","lhy.undernation.ads.SubAdlibAdViewShallWeAd");

		AdlibConfig.getInstance().setAdlibKey(idAdlibr);
		AdlibConfig.getInstance().bindPlatform("ADAM", packageName + ".ads.SubAdlibAdViewAdam");
		AdlibConfig.getInstance().bindPlatform("CAULY", packageName + ".ads.SubAdlibAdViewCauly");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}
}
