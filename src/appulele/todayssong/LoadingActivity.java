package appulele.todayssong;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import appulele.todayssong.json.GetSongDataTask;
import appulele.todayssong.json.GetSongListTask;

import com.mocoplex.adlib.AdlibConfig;

public class LoadingActivity extends BaseActivity {
	private final String TAG = this.getClass().getSimpleName();
	boolean Debug = true;
	String packageName;
	String idAdlibr;
	Context mContext;
	private static ArrayList<SongData> songList;

	/** Called when the activity is first created. */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.imageviewer);

		packageName = getPackageName();
		idAdlibr = getResources().getString(R.string.ad_adlibr);
		mContext = this;
		initAds();

		// 기본 데이터를 불러온다. 곡명, 노래명, 앨범ID 가수ID
		GetSongListTask songListTask = new GetSongListTask(mContext);
		songListTask.execute();

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == 0) {
			finish();
		}
	}

	// 광고 초기화 함수
	protected void initAds() {
		if (Debug) {
			Log.d(TAG, "--initAds Start--");
			// Log.d(TAG, "-INMOBI : " + packageName +
			// ".ads.SubAdlibAdViewInmobi");
			Log.d(TAG, "-ADAM : " + packageName + ".ads.SubAdlibAdViewAdam");
			Log.d(TAG, "-CAULY : " + packageName + ".ads.SubAdlibAdViewCauly");
			Log.d(TAG, "--initAds End--");
		}

		// AdlibConfig.getInstance().bindPlatform("INMOBI", packageName +
		// ".ads.SubAdlibAdViewInmobi");
		// 쓰지 않을 광고플랫폼은 삭제해주세요.

		// AdlibConfig.getInstance().bindPlatform("ADMOB","lhy.undernation.ads.SubAdlibAdViewAdmob");
		// AdlibConfig.getInstance().bindPlatform("TAD","lhy.undernation.ads.SubAdlibAdViewTAD");
		// AdlibConfig.getInstance().bindPlatform("NAVER","lhy.undernation.ads.SubAdlibAdViewNaverAdPost");
		// AdlibConfig.getInstance().bindPlatform("SHALLWEAD","lhy.undernation.ads.SubAdlibAdViewShallWeAd");

		AdlibConfig.getInstance().setAdlibKey(idAdlibr);
		AdlibConfig.getInstance().bindPlatform("ADAM", packageName + ".ads.SubAdlibAdViewAdam");
		AdlibConfig.getInstance().bindPlatform("CAULY", packageName + ".ads.SubAdlibAdViewCauly");
	}

	public void addSongList(ArrayList<SongData> songList) {
		this.setList(songList);
		// 추가적으로 앨범, 가수 이미지 URL, 앨범 발매일 등을 저장
		GetSongDataTask songDataTask = new GetSongDataTask(mContext, songList);
		songDataTask.execute();

	}

	public static ArrayList<SongData> getSongDataList() {
		return songList;
	}

	public void setList(ArrayList<SongData> songDataList) {
		LoadingActivity.songList = songDataList;

	}

	public void startMain() {
		Intent intent = new Intent(LoadingActivity.this, MainActivity.class);
		// startActivity(intent);
		startActivityForResult(intent, 0);
	}
}
