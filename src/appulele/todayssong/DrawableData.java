package appulele.todayssong;

import java.util.ArrayList;

import android.R.array;
import android.graphics.drawable.Drawable;
import android.util.Log;

public class DrawableData {
	private Drawable img;
	private int position;
	private String[] singerIds;
	private ArrayList<String> singerNames; 
	private int singerIndex;
	String TAG = getClass().getSimpleName();

	public DrawableData(Drawable img, int index, String[] singerIDs,int singerIndex ) {
		this.setImg(img);
		this.setPosition(index);
		this.singerIds = singerIDs;
		singerNames = new ArrayList<String>();
		this.setSingerIndex(singerIndex);
		

	}

	public Drawable getImg() {
		return img;
	}

	public void setImg(Drawable img) {
		this.img = img;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String[] getSingerIds() {
		return singerIds;
	}

	public void setSingerId(String[] singerId) {
		this.singerIds = singerId;
	}

	public ArrayList<String> getSingerNames() { 
		return singerNames; 
	}

	public void addSingerNames(ArrayList<String> singerNames) {
		this.singerNames = singerNames;
		
	}
 

	public int getSingerIndex() {
		return singerIndex;
	}

	public void setSingerIndex(int singerIndex) {
		this.singerIndex = singerIndex;
	}
}
