package appulele.todayssong;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import appulele.todayssong.json.OuterImageUrlTask;
import appulele.todayssong.json.URLs;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.mocoplex.adlib.AdlibConfig;

public class MainActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks, OnInitializedListener, OnTouchListener {
	public static final String TAG = "MAIN ACTIVITY";
	public static final int INFOMATION = 0;
	public static final int IMAGE = 1;
	public static final int HOMEPAGE = 2;
	public static final int GOYOUTUBE = 3;

	private ViewFlipper viewFlipper;
	ArrayList<AddViewPage> viewPageList;
	ArrayList<SongData> songList;
	// AppSectionsPagerAdapter mAppSectionsPagerAdapter;

	private NavigationDrawerFragment mNavigationDrawerFragment;
	private static CharSequence mTitle;
	String packageName;
	String idAdlibr;
	public static final String DEVELOPER_KEY = "AIzaSyCyxcr3nQPfpDpC159JvKBNro2U5r1zCDE";

	int mPreTouchPosX;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		inits();

		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// 광고
		packageName = getPackageName();
		idAdlibr = getResources().getString(R.string.ad_adlibr);
		initAds();

		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
	}

	private void inits() {
		mContext = this;
		// viewflipper inits
		viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper);
		viewFlipper.setOnTouchListener(this);
		viewPageList = new ArrayList<>();

		// 로딩 액티비티에서 만든 노래리스트 틀을 불러온다.
		songList = LoadingActivity.getSongDataList();

		// // 로딩 액티비티에서 만든 노래 리스틍의 세부 내용을 채워 넣는다.
		setExternalImg();

	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// update the main content by replacing fragments
		// FragmentManager fragmentManager = getSupportFragmentManager();
		// fragmentManager.beginTransaction().replace(R.id.container,
		// PlaceholderFragment.newInstance(position + 1)).commit();
		// fragmentManager.beginTransaction().replace(R.id.container,
		// R.id.fragment_singer).commit();
		switch (position) {

		case INFOMATION:
			mTitle = getString(R.string.title_section1);
			Toast.makeText(mContext, mTitle, Toast.LENGTH_LONG).show();
			break;
		case IMAGE:
			mTitle = getString(R.string.title_section2);

			Toast.makeText(mContext, mTitle, Toast.LENGTH_LONG).show();
			break;
		case HOMEPAGE:
			mTitle = getString(R.string.title_section3);
			Toast.makeText(mContext, mTitle, Toast.LENGTH_LONG).show();
			break;
		case GOYOUTUBE:
			mTitle = getString(R.string.title_section4);
			LinearLayout curView = (LinearLayout) viewFlipper.getCurrentView();
			int index = (int) curView.getTag();
			SongData data = viewPageList.get(index).getSongData();
			String url = URLs.youtubeWebFindAQuery+data.getSingerName()+"+"+data.getSongName();
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			startActivity(intent);
			break;

		}

	}

	public void onSectionAttached(int number) {
		switch (number) {
		case 1:
			mTitle = getString(R.string.title_section1);
			LinearLayout page = (LinearLayout) viewFlipper.getCurrentView();

			break;
		case 2:
			mTitle = getString(R.string.title_section2);
			break;
		case 3:
			mTitle = getString(R.string.title_section3);
			break;
		case 4:
			mTitle = getString(R.string.title_section4);
			break;
		}
	}

	public void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.main, menu);

			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";

		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static PlaceholderFragment newInstance(int sectionNumber) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

			// 유튜브
			// 플래그먼트=========================================================================================================
			YouTubePlayerFragment mYoutubeFragment = new YouTubePlayerFragment();
			mYoutubeFragment.initialize("AIzaSyCyxcr3nQPfpDpC159JvKBNro2U5r1zCDE", new YouTubePlayer.OnInitializedListener() {

				@Override
				public void onInitializationSuccess(Provider arg0, YouTubePlayer arg1, boolean arg2) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onInitializationFailure(Provider arg0, YouTubeInitializationResult arg1) {
					// TODO Auto-generated method stub

				}
			});
			// initialize("AIzaSyCyxcr3nQPfpDpC159JvKBNro2U5r1zCDE",
			// YouTubePlayer.OnInitializedListener listener);
			// ==========================================================================================================================
			View rootView = inflater.inflate(R.layout.view_main, container, false);
			TextView textView = (TextView) rootView.findViewById(R.id.section_label);
			textView.setText(Integer.toString(getArguments().getInt(ARG_SECTION_NUMBER)));

			return rootView;
		}

		@Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);
			((MainActivity) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
		}
	}

	// 광고 초기화 함수
	protected void initAds() {
		// if (Debug) {
		// Log.d(TAG, "--initAds Start--");
		// // Log.d(TAG, "-INMOBI : " + packageName +
		// // ".ads.SubAdlibAdViewInmobi");
		// Log.d(TAG, "-ADAM : " + packageName + ".ads.SubAdlibAdViewAdam");
		// Log.d(TAG, "-CAULY : " + packageName + ".ads.SubAdlibAdViewCauly");
		// Log.d(TAG, "--initAds End--");
		// }

		// AdlibConfig.getInstance().bindPlatform("INMOBI", packageName +
		// ".ads.SubAdlibAdViewInmobi");
		// 쓰지 않을 광고플랫폼은 삭제해주세요.

		// AdlibConfig.getInstance().bindPlatform("ADMOB","lhy.undernation.ads.SubAdlibAdViewAdmob");
		// AdlibConfig.getInstance().bindPlatform("TAD","lhy.undernation.ads.SubAdlibAdViewTAD");
		// AdlibConfig.getInstance().bindPlatform("NAVER","lhy.undernation.ads.SubAdlibAdViewNaverAdPost");
		// AdlibConfig.getInstance().bindPlatform("SHALLWEAD","lhy.undernation.ads.SubAdlibAdViewShallWeAd");

		AdlibConfig.getInstance().setAdlibKey(idAdlibr);
		AdlibConfig.getInstance().bindPlatform("ADAM", packageName + ".ads.SubAdlibAdViewAdam");
		AdlibConfig.getInstance().bindPlatform("CAULY", packageName + ".ads.SubAdlibAdViewCauly");
	}

	@Override
	public void onInitializationFailure(Provider arg0, YouTubeInitializationResult arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onInitializationSuccess(Provider arg0, YouTubePlayer arg1, boolean arg2) {
		// TODO Auto-generated method stub

	}

	/**
	 * ViewFlipper 다음 뷰 이동
	 */
	public void MoveNextView() {
		viewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.appear_from_right));
		viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.disappear_to_left));
		viewFlipper.showNext();
		int position = viewFlipper.getDisplayedChild();

	}

	/**
	 * ViewFlipper 이전 뷰 이동
	 */
	public void MovePreviousView() {
		viewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.appear_from_left));
		viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.disappear_to_right));
		viewFlipper.showPrevious();
		int position = viewFlipper.getDisplayedChild();

	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		boolean value = true;
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			mPreTouchPosX = (int) event.getX();
		} else if (event.getAction() == MotionEvent.ACTION_UP) {
			int nTouchPosX = (int) event.getX();
			if (nTouchPosX < mPreTouchPosX && Math.abs(mPreTouchPosX - nTouchPosX) > 210) {
				MoveNextView();
				Log.d(TAG, "viewFlipper ontouch next");
				value = true;
			} else if (nTouchPosX > mPreTouchPosX && Math.abs(mPreTouchPosX - nTouchPosX) > 210) {
				MovePreviousView();
				Log.d(TAG, "viewFlipper ontouch before");
				value = true;
			}
		} else {
			value = false;
		}
		// int positionVFlipper = viewFlipper.getDisplayedChild();
		Log.d(TAG, "viewFlipper ontouch");
		return value;
	}

	// list에 담겨진 정보로 페이지를 생성
	public void addViewFlipperPage(ArrayList<SongData> list) {
		SongData data = null;

		for (int i = 0; i < list.size(); i++) {
			data = list.get(i);
			Log.d("tag", "i:" + i + ",  data: " + data.getSingerName() + data.getSongName() + data.getAlbumURL() + data.getSingerImgURL());
			AddViewPage page = new AddViewPage(mContext, data, i);
			viewPageList.add(page);
			//
		}
	}

	public ViewFlipper getViewFlipper() {
		return viewFlipper;
	}

	public void setViewFlipper(ViewFlipper viewFlipper) {
		this.viewFlipper = viewFlipper;
	}

	/**
	 * 이미지를 외부에서 불러와서 봉여준다.
	 */
	public void setExternalImg() {
		OuterImageUrlTask imgUrlTask = new OuterImageUrlTask(mContext, songList);
		imgUrlTask.execute();
	}

	public Drawable setOuterImg() {

		return null;
	}
}
