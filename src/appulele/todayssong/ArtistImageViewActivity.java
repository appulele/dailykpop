package appulele.todayssong;

import java.util.ArrayList;
import java.util.zip.Inflater;

import com.mocoplex.adlib.AdlibConfig;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import appulele.todayssong.json.OuterArtistImageUrlTask;

public class ArtistImageViewActivity extends BaseActivity {
	String packageName;
	String idAdlibr;
	Context mContext;
	GridView gvGallery;
	ArrayList<DrawableData> list;
	GridAdapter adapter;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar actionBar = getActionBar();
		actionBar.hide();
		setContentView(R.layout.artist_img_list);
		packageName = getPackageName();
		idAdlibr = getResources().getString(R.string.ad_adlibr);
		mContext = this;
		initAds(); 

		Intent intent = getIntent();
		String[] artistIDs = intent.getStringArrayExtra("artistIds");
	 

		list = new ArrayList<>();
		gvGallery = (GridView) findViewById(R.id.gridViewArtistGallery);
		adapter = new GridAdapter(mContext, list);
		gvGallery.setAdapter(adapter);
		OuterArtistImageUrlTask task = new OuterArtistImageUrlTask(mContext, artistIDs);
		task.execute();
	}

	/**
	 * 이미지 추가하기(이미지와 position값으로 묶인 arraylist 이용)
	 * @param drawableList
	 */
	public void addImg(ArrayList<DrawableData> drawableList) {
 
		for (int i = 0; i < drawableList.size(); i++) {
//			Drawable img = drawableList.get(i).getImg();
//			int position = drawableList.get(i).getPosition(); 
			list.add(drawableList.get(i));
			 
		}
		adapter.notifyDataSetChanged();

	}
	// 광고 초기화 함수
		protected void initAds() { 
			// AdlibConfig.getInstance().bindPlatform("INMOBI", packageName +
			// ".ads.SubAdlibAdViewInmobi");
			// 쓰지 않을 광고플랫폼은 삭제해주세요. 
			// AdlibConfig.getInstance().bindPlatform("ADMOB","lhy.undernation.ads.SubAdlibAdViewAdmob");
			// AdlibConfig.getInstance().bindPlatform("TAD","lhy.undernation.ads.SubAdlibAdViewTAD");
			// AdlibConfig.getInstance().bindPlatform("NAVER","lhy.undernation.ads.SubAdlibAdViewNaverAdPost");
			// AdlibConfig.getInstance().bindPlatform("SHALLWEAD","lhy.undernation.ads.SubAdlibAdViewShallWeAd");

			AdlibConfig.getInstance().setAdlibKey(idAdlibr);
			AdlibConfig.getInstance().bindPlatform("ADAM", packageName + ".ads.SubAdlibAdViewAdam");
			AdlibConfig.getInstance().bindPlatform("CAULY", packageName + ".ads.SubAdlibAdViewCauly");
		}
}
