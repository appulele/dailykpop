package appulele.todayssong;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.R.drawable;
import android.graphics.drawable.Drawable;

public class SongData {
	//로딩에서 구현부분
	private String singerName="";
	private	String songName="";
	private String albumURL="";
	private String songURL="";
	private String[] singerURL;
	//메인에서 구현부분
	private String songImgURL="";
	private String singerImgURL=""; 
	private String albumName="";
	private String albumDate="";
	private Drawable singerImg;
	private Drawable nextSingerImg;
	private Drawable beforeSingerImg;
	


	
	public SongData(String SongName,String SingerName ) {
	 this.songName = SongName;
	 this.singerName = SingerName;
	}

	public String getSongImgURL() {
		return songImgURL;
	}

	public void setSongImgURL(String songImgURL) {
		this.songImgURL = songImgURL;
	}

	public String getSingerImgURL() {
		return singerImgURL;
	}

	public void setSingerImgURL(String singerImgURL) {
		this.singerImgURL = singerImgURL;
	}

	public String getSingerName() {
		return singerName;
	}

	public void setSingerName(String singerName) {
		this.singerName = singerName;
	}

	public String getSongName() {
		return songName;
	}

	public void setSongName(String songName) {
		this.songName = songName;
	}

	public String getAlbumName() {
		return albumName;
	}

	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}

	public String getAlbumURL() {
		return albumURL;
	}

	public void setAlbumURL(String albumURL) {
		this.albumURL = albumURL;
	}

	public String getSongURL() {
		return songURL;
	}

	public void setSongURL(String songURL) {
		this.songURL = songURL;
	}

	public String getAlbumDate() {
		return albumDate;
	}

	public void setAlbumDate(String albumDate) {
		this.albumDate = albumDate;
	}

	public Drawable getSingerImg() {
		return singerImg;
	}

	public void setSingerImg(Drawable drawable) {
		this.singerImg = drawable;
	}

	public Drawable getNextSingerImg() {
		return nextSingerImg;
	}

	public void setNextSingerImg(Drawable nextSingerImg) {
		this.nextSingerImg = nextSingerImg;
	}

	public Drawable getBeforeSingerImg() {
		return beforeSingerImg;
	}

	public void setBeforeSingerImg(Drawable beforeSingerImg) {
		this.beforeSingerImg = beforeSingerImg;
	}

	public String[] getSingerURLs() {
		return singerURL;
	}

	public void setSingerURLs(String[] singerURL) {
		this.singerURL = singerURL;
	}

}
