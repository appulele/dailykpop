package appulele.todayssong.extermalImg;

import java.io.InputStream;
import java.net.URL;

import android.graphics.drawable.Drawable;

public class externalDrawable {
	/**
	 * 외부이미지 쓰기
	 * @param url
	 * @return
	 */
		public static Drawable LoadImageFromWebOperations(String url) {
			try {
				InputStream is = (InputStream) new URL(url).getContent();
				Drawable d = Drawable.createFromStream(is, "src name");
				return d;
			} catch (Exception e) {
				System.out.println(e);
				return null;
			}
		}

}
